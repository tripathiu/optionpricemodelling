#! /usr/bin/env python3

import numpy as np
from scipy.stats import norm
from scipy.optimize import minimize_scalar
from collections import namedtuple

N = norm.cdf   # cdf of normal distribution
exp = np.exp
sqrt = np.sqrt
log = np.log

class OptType:
    CALL = 'CE'
    PUT  = 'PE'

Greeks = namedtuple("Greeks", "delta gamma vega theta")

# get synthetic futures price from call put parity
# in efficient markets,
# futPrice = callPrice - putPrice + strikePrice
def getSyntheticFuturesPrice(c,p,k):
    return c-p +k

# s     = underlying price
# k     = strike price
# r     = risk free rate
# t     = time till expiry
# sigma = annualized volatility


def getBSPrice(optType, s, k, t, r, sigma):
    d1 = (np.log(s/k) + (r + sigma**2/2)*t) / (sigma*np.sqrt(t))
    d2 = d1 - sigma*np.sqrt(t)

    if optType == OptType.CALL:
        return s*N(d1) - k*np.exp(-r*t) * N(d2)
    elif optType == OptType.PUT:
        return -s*N(-d1) + k*np.exp(-r*t) * N(-d2)

def getBSSigma(optType, s, k, t, r, price):
    def objective(sigma):
        return (price - getBSPrice(optType, s, k, t, r, sigma))**2
    return minimize_scalar(objective)

def getB76Price(optType, f, k, t, r, sigma):
    if t == 0 or sigma == 0:
        d1 = np.inf
    else:
        d1 = (np.log(f/k) + ((sigma**2)/2)*t) / (sigma*np.sqrt(t))
    d2 = d1 - sigma*np.sqrt(t)

    if optType == OptType.CALL:
        return (f*N(d1) - k*N(d2)) * np.exp(-r*t)
    elif optType == OptType.PUT:
        return (-f*N(-d1) + k*N(-d2)) * np.exp(-r*t)

def getB76Sigma(optType, f, k, t, r, price):
    def objective(sigma):
        return (price - getB76Price(optType, f, k, t, r, sigma))**2
    return minimize_scalar(objective) #, bounds=(1e-15, 1e15))

def getB76Info(optType, f, k, t, r, price):
    sigma = getB76Sigma(optType, f, k, t, r, price).x

    if t == 0:
        d1 = np.inf
    else:
        d1 = (np.log(f/k) + ((sigma**2)/2)*t) / (sigma*np.sqrt(t))
    d2 = d1 - sigma*np.sqrt(t)


    if optType == OptType.CALL:
        delta = exp(-r*t)*N(d1)
    elif optType == OptType.PUT:
        delta = exp(-r*t)*(N(d1)-1)

    if t == 0:
        gamma = 0
    else:
        gamma = exp(-r*t)*N(d1) / (f*sigma*sqrt(t))

    vega  = exp(-r*t)*f*N(d1)*sqrt(t)

    if t == 0:
        theta = 0
    elif optType == OptType.CALL:
        theta = exp(-r*t) * ((-f*N(d1)*sigma/(2*sqrt(t)) + r*f*N(d1) - r*k*N(d2)))
    elif optType == OptType.PUT:
        theta = exp(-r*t) * ((-f*N(d1)*sigma/(2*sqrt(t)) - r*f*N(-d1) + r*k*N(-d2)))
    greeks = Greeks(delta, gamma, vega/100, theta/365)

    iv = sigma*100;
    return (iv, greeks)

