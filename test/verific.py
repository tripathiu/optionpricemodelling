from bscalc import *
import datetime

fut = 17560
spot = 17465


# def getB76Sigma(optType, f, k, t, r, price):

strike = 17800

cSigma = getB76Sigma(OptType.CALL, fut, strike, 32/365, 0, 222.55)
pSigma = getB76Sigma(OptType.PUT, fut, strike, 32/365, 0, 263.10)
print(cSigma)
print(pSigma)

print((cSigma.x + pSigma.x )/ 2)

# def getB76Price(optType, f, k, t, r, sigma):
# p = getB76Price(OptType.CALL, 17510, 17600, 32.0/365.0, 0, 11.6)
# print(p)

myprice = getBSPrice(OptType.CALL, spot, strike, 33/365, 0.1, 9.91/100);
mysigma = getBSSigma(OptType.CALL, spot, strike, 33/365, 0.1, myprice);
print(myprice, mysigma.x*100)

mysigma = getB76Sigma(OptType.CALL, fut, strike, 33/365, 0, 133.00);
myprice = getB76Price(OptType.CALL, fut, strike, 33/365, 0, mysigma.x);
print(myprice, mysigma.x*100)

expiry = datetime.date(2023,3,29)
now = datetime.datetime.today().date()
yearsTillExpiry = (expiry-now).days/365
print(f"{yearsTillExpiry=}")

price = getB76Price(OptType.CALL, fut, 17800, yearsTillExpiry, 0, 11.0/100)
iv, greeks = getB76Info(OptType.CALL, f=fut, k=17800, t=yearsTillExpiry, r=0, price=142.30)
print(price)
print(f"\n{iv=}|11.0\n{greeks.gamma=:.04}|0.0006\n{greeks.vega=}|20\n{greeks.theta=:.02}|-3.3\n{greeks.delta=:.02}|0.35")
# sigmaPut = getB76Sigma(OptType.PUT, fut, strike, 33/365, 0.1, 372);
