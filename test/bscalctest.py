from bscalc import *
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

K = 100
r = 0
T = 1
sigma = 0.3

S = np.arange(60,140,0.1)

calls = [getB76Price(OptType.CALL, s, K, T, r, sigma) for s in S]
puts = [getB76Price(OptType.PUT, s, K, T, r, sigma) for s in S]
plt.plot(S, calls, label='Call Value')
plt.plot(S, puts, label='Put Value')
plt.xlabel('$S_0$')
plt.ylabel(' Value')
plt.legend()
plt.show()

print(getB76Price(OptType.CALL, 100, 110, 1, 0, 10))
print(getB76Sigma(OptType.CALL, 100, 110, 1, 0, 74.71))
