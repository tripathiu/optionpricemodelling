import nsepy as nse
from datetime import date
import matplotlib.pyplot as plt
import bscalc as bs

startDate = date(2023,1,1)
expiryDate = date(2023,2,23)


spot = nse.get_history(symbol="NIFTY",
                start=startDate,
                end=date.today(),
                index=True)

fut = nse.get_history(symbol="NIFTY",
                      start=startDate,
                      end=date.today(),
                      index=True,
                      futures=True,
                      expiry_date=expiryDate)

with open(f'data/nifty.csv', 'w') as f:
    spot.to_csv(f)

with open(f'data/niftyfut.csv', 'w') as f:
    fut.to_csv(f)

opt17800ce = nse.get_history(symbol="NIFTY",
                  start=startDate,
                  end=expiryDate,
                  index=True,
                  option_type='CE',
                  strike_price=17800,
                  expiry_date=expiryDate)

print(spot["Close"])
print(opt17800ce["Close"])


plt.plot(spot["Close"])
plt.plot(opt17800ce["Close"])

plt.show()

#getNseData

expiryDate = date(2023,2,23)

cOpts = {}
pOpts = {}
strikes = range(16500, 19500, 100)
for strike in strikes:
    cOpts[strike] = nse.get_history(symbol="NIFTY",
                                    start=startDate,
                                    end=expiryDate,
                                    index=True,
                                    option_type='ce',
                                    strike_price=strike,
                                    expiry_date=expiryDate)
    pOpts[strike] = nse.get_history(symbol="NIFTY",
                                    start=startDate,
                                    end=expiryDate,
                                    index=True,
                                    option_type='pe',
                                    strike_price=strike,
                                    expiry_date=expiryDate)
    print(f'{strike} done')

for strike in strikes:
    with open(f'data/{strike}ce.csv', 'w') as f:
        cOpts[strike].to_csv(f)
    with open(f'data/{strike}pe.csv', 'w') as f:
        pOpts[strike].to_csv(f)

