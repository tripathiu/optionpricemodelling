# OptionPriceModelling

- Syncing live market option price with live market feed.
- Calculate greeks for single options and calendar spread strategies
- Calculate payoff on and before expiry
- Create the best strategies and backtest on market data

